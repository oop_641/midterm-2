public class CinemaApp {
    private static void TicketMenu() {
        System.out.println("|---------------------------|");
        System.out.println("| position A price 180 Bath |");
        System.out.println("| position B price 220 Bath |");
        System.out.println("| position C price 280 Bath |");
        System.out.println("|---------------------------|");
    }

    private static void PopcornMenu() {
        System.out.println("|-------------------------------------|");
        System.out.println("| Popcron size 120 oz. price 120 Bath |");
        System.out.println("| Popcron size 180 oz. price 160 Bath |");
        System.out.println("| Popcron size 240 oz. price 200 Bath |");
        System.out.println("|-------------------------------------|");
    }

    public static void main(String[] args) {
        TicketMenu();
        TicketPrice HarryPotter = new TicketPrice("Frame", 'A', 180);
        HarryPotter.ShowSelectPosition();
        HarryPotter.pay(200);
        System.out.println();
        TicketPrice Doraemon = new TicketPrice("Mod", 'C', 280);
        Doraemon.ShowSelectPosition();
        Doraemon.pay(200);
        System.out.println();
        TicketPrice Spiderman = new TicketPrice("Kong", 'B', 220);
        Spiderman.ShowSelectPosition();
        Spiderman.pay(300);
        System.out.println();
        PopcornMenu();
        PopcornPrice Sweet = new PopcornPrice("Frame", "120 oz.", 120);
        Sweet.ShowSelectType();
        Sweet.pay(120);
        System.out.println();
        PopcornPrice Salty = new PopcornPrice("Mod", "240 oz.", 200);
        Salty.ShowSelectType();
        Salty.pay(500);
        System.out.println();
        PopcornPrice Cheese = new PopcornPrice("Kong", "180 oz.", 160);
        Cheese.ShowSelectType();
        Cheese.pay(159);

    }

}
