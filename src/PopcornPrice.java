public class PopcornPrice {
    private String name;
    private String size;
    private double price;
    private double change;

    public PopcornPrice(String name, String size, double price) {
        this.size = size;
        this.price = price;
        this.change = change;
        this.name = name;
    }

    public boolean pay(double money) {
        if (money < price) {
            System.out.println("Your Money : " + "" + money + " " + "Bath");
            System.out.println("Unable to pay !!!");
            return false;
        }
        change = money - price;
        System.out.println("Your Money : " + "" + money + " " + "Bath");
        System.out.println("Your Change : " + "" + change + " " + "Bath");
        return true;
    }

    public void ShowSelectType() {
        System.out.println("" + name +  " select Popcorn size : " + "" + size);
    }

}
